Rails.application.routes.draw do
  root 'welcome#index'

  get 'jobs' => 'welcome#jobs'

  post 'create_job' => 'welcome#create_job'
end
