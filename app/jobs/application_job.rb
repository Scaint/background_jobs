class ApplicationJob < ActiveJob::Base

  before_enqueue do |job|
    JobStatus.set_job_status(job, JobStatus::ENQUEUED)
  end

  around_perform do |job, block|
    JobStatus.set_job_status(job, JobStatus::STARTED)

    block.call

    JobStatus.set_job_status(job, JobStatus::FINISHED)
  end

end
