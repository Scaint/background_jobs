require 'net/http'

module FayeHelper
  HOST = "http://localhost:#{ENV['FAYE_PORT'] || 3000}"
  
  def self.url
    mountpoint = FayeRails::Middleware::DEFAULTS.fetch(:mount)
    HOST + mountpoint
  end

  def self.push(channel, data = nil, &block)
    message = {
      channel: channel,
      data: block_given? ? capture(&block) : data
    }

    Net::HTTP.post_form(URI.parse(url), message: message.to_json)
  end
end
