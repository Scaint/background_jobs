//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require popover-extra-placements.min
//= require angular
//= require faye
//= require angular-faye
//= require_tree .

$(function () {
  $('[data-toggle="popover"]').popover()
})
