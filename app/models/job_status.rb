class JobStatus

  REDIS_PREFIX = 'aj'
  STATUSES = [ENQUEUED = 1, STARTED = 2, FINISHED = 3]
  FINISHED_TTL = 5 # seconds

  attr_reader :job_class_name, :job_id, :status

  def initialize(key)
    @key = key
    _, @job_class_name, @job_id = @key.split(':')
    @status, @changed_at = redis.hgetall(@key).values_at('status', 'changed_at')
  end

  def enqueued?
    status.to_i == ENQUEUED
  end

  def started?
    status.to_i == STARTED
  end

  def finished?
    status.to_i == FINISHED
  end

  def destroy
    self.redis.del(key)
  end

  def self.set_job_status(job, status)
    raise ArgumentError, 'Invalid job status' unless STATUSES.include?(status)

    key = "#{REDIS_PREFIX}:#{job.class.name}:#{job.job_id}"
    redis.mapped_hmset(key, 'status' => status, 'changed_at' => Time.now.to_i)
    redis.expire(key, FINISHED_TTL) if status == FINISHED

    FayeHelper.push('/test', 'job_status_changed')
  end

  def self.all(job_class_name = '*')
    self.keys("#{job_class_name}:*").map { |key| self.new(key) }
  end

  def self.flush
    self.keys('*:*').map { |key| redis.del(key) }
  end

  def self.keys(pattern)
    redis.keys(REDIS_PREFIX + ':' + pattern)
  end

private

  def redis
    self.class.redis
  end

  def self.redis
    @redis ||= Redis.current
  end

end
