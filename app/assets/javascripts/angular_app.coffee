app = angular.module 'app', ['faye']

app.factory 'Faye', ['$faye', ($faye) ->
  $faye(FAYE_URL)
]

app.controller 'JobsController', ['$scope', '$http', '$timeout', 'Faye', ($scope, $http, $timeout, Faye) ->
  $scope.jobs = []
  $scope.status_text = ['unknown', 'enqueued', 'started', 'finished']
  $scope.status_class = ['default', 'default', 'info', 'success']

  @loadJobs = ->
    $http.get('/jobs')
      .success (response) ->
        $timeout ->
          $scope.$apply ->
            $scope.jobs = response.jobs

  @loadJobs()
  
  Faye.subscribe '/test', =>
    @loadJobs()
]
