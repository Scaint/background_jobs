class WelcomeController < ApplicationController

  def index; end

  def jobs
    jobs = JobStatus.all
      .map { |job| job.as_json(except: ['key']) }
      .sort { |a, b| b['changed_at'] <=> a['changed_at'] }

    render json: {jobs: jobs}
  end

  def create_job
    SomeJob.perform_later

    render nothing: true
  end

end
